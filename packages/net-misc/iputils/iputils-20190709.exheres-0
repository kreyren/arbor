# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require github [ tag="s${PV}" ] meson

SUMMARY="Small useful utilities for networking"
HOMEPAGE+=" https://wiki.linuxfoundation.org/networking/${PN}"
LICENCES="BSD-4 || ( GPL-2 GPL-3 ) as-is"

SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    caps
    idn
    ( providers: gcrypt kernel-crypto libressl nettle openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        app-text/docbook-xsl-ns-stylesheets
        dev-libs/libxslt [[ note = [ xsltproc ] ]]
        sys-devel/gettext
    build+run:
        caps? ( sys-libs/libcap )
        idn? ( net-dns/libidn2:= )
        providers:gcrypt? (
            dev-libs/libgcrypt
            dev-libs/libgpg-error
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:nettle? ( dev-libs/nettle )
        providers:openssl? ( dev-libs/openssl )
    suggestion:
        net-analyzer/traceroute [[
            description = [ Traceroute is a tool commonly categorized with iputils ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-ninfod-change-variable-name-to-avoid-colliding-with-.patch
)

src_configure() {
    local meson_params=(
        -DBUILD_ARPING=true
        -DBUILD_RARPD=true
        -DUSE_GETTEXT=true
        $(meson_switch caps USE_CAP)
        $(meson_switch idn USE_IDN)
    )

    if option providers:gcrypt ; then
        meson_params+=( -DUSE_CRYPTO=gcrypt )
    elif option providers:kernel-crypto ; then
        meson_params+=( -DUSE_CRYPTO=kernel )
    elif (option providers:libressl || option providers:openssl ) ; then
        meson_params+=( -DUSE_CRYPTO=openssl )
    elif option providers:nettle ; then
        meson_params+=( -DUSE_CRYPTO=nettle )
    fi

    exmeson "${meson_params[@]}"
}

pkg_postinst() {
    if option caps; then
        nonfatal edo setcap cap_net_raw+ep /usr/$(exhost --target)/bin/arping
        nonfatal edo setcap cap_net_raw+ep /usr/$(exhost --target)/bin/clockdiff
        nonfatal edo setcap cap_net_raw+ep /usr/$(exhost --target)/bin/ping
    fi
}

