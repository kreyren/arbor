# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A library for making GPG easier to use"
HOMEPAGE="https://www.gnupg.org/related_software/${PN}"
DOWNLOADS="mirror://gnupg/${PN}/${PNV}.tar.bz2"

LICENCES="
    || ( GPL-2 LGPL-2.1 )
    qt5? ( GPL-2 )
"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    doc [[ description = [ Build API docs for the Qt bindings ] ]]
    qt5

    doc [[ requires = qt5 ]]
"

# Tests are disabled via --disable-gpg{,sm}-test configure switches
# below because they run during compile phase, last checked: 1.12.0
RESTRICT="test"

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            media-gfx/graphviz
        )
        virtual/pkg-config
    build+run:
        app-crypt/gnupg[>=2.0.4]
        dev-libs/libassuan[>=2.4.2]
        dev-libs/libgpg-error[>=1.24]
        qt5? ( x11-libs/qtbase:5 )
"

DEFAULT_SRC_COMPILE_PARAMS=(
    CC_FOR_BUILD=$(exhost --build)-cc
)

src_configure() {
    # additional available language bindings: JavaScript, python, python2, python3
    local languages="cl,cpp"
    option qt5 && languages+=",qt"
    econf \
        --enable-languages="${languages}" \
        --disable-gpg-test \
        --disable-gpgsm-test \
        --with-libassuan-prefix=/usr/$(exhost --target) \
        --with-libgpg-error-prefix=/usr/$(exhost --target) \
        $(option doc ac_cv_prog_DOXYGEN=doxygen ac_cv_prog_DOXYGEN=)
}

src_test() {
    # Allow the tests to run its own instance of gpg-agent
    esandbox allow_net "unix:${WORK}/tests/gpg/S.gpg-agent*"
    esandbox allow_net --connect "unix:${WORK}/tests/gpg/S.gpg-agent*"
    esandbox allow_net "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox allow_net --connect "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox allow_net "unix:${WORK}/tests/gpg*/S.dirmngr*"
    esandbox allow_net --connect "unix:${WORK}/tests/gpg*/S.dirmngr*"

    if option qt5 ; then
        esandbox allow_net "unix:${WORK}/lang/qt/tests/S.gpg-agent*"
        esandbox allow_net --connect "unix:${WORK}/lang/qt/tests/S.gpg-agent*"
        esandbox allow_net "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
        esandbox allow_net --connect "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
        esandbox allow_net "unix:${TEMP}/t-various-*/S.gpg-agent*"
        esandbox allow_net --connect "unix:${TEMP}/t-various-*/S.gpg-agent*"
        esandbox allow_net "unix:${WORK}/lang/qt/tests/S.dirmngr*"
        esandbox allow_net --connect "unix:${WORK}/lang/qt/tests/S.dirmngr*"
    fi

    default

    if option qt5 ; then

        esandbox disallow_net --connect "unix:${WORK}/lang/qt/tests/S.dirmngr*"
        esandbox disallow_net "unix:${WORK}/lang/qt/tests/S.dirmngr*"
        esandbox disallow_net --connect "unix:${TEMP}/t-various-*/S.gpg-agent*"
        esandbox disallow_net "unix:${TEMP}/t-various-*/S.gpg-agent*"
        esandbox disallow_net --connect "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
        esandbox disallow_net "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
        esandbox disallow_net --connect "unix:${WORK}/lang/qt/tests/S.gpg-agent*"
        esandbox disallow_net "unix:${WORK}/lang/qt/tests/S.gpg-agent*"
    fi

    esandbox disallow_net --connect "unix:${WORK}/tests/gpg*/S.dirmngr*"
    esandbox disallow_net "unix:${WORK}/tests/gpg*/S.dirmngr*"
    esandbox disallow_net --connect "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox disallow_net "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox disallow_net --connect "unix:${WORK}/tests/gpg/S.gpg-agent*"
    esandbox disallow_net "unix:${WORK}/tests/gpg/S.gpg-agent*"
}

src_install() {
    default

    if option doc ; then
        edo pushd lang/qt/doc/generated
        dodoc -r html
        edo popd
    fi
}

