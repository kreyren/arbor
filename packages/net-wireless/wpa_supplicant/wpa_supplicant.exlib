# Copyright 2009 Daniel Mierswa <impulze@impulze.org>
# Copyright 2010 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="git://w1.fi/srv/git/hostap.git"
    require scm-git
else
    DOWNLOADS="https://w1.fi/releases/${PNV}.tar.gz"
fi

require qmake [ slot=5 ] gtk-icon-cache systemd-service
require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]

export_exlib_phases src_prepare src_configure src_compile src_install pkg_postinst pkg_postrm

SUMMARY="Linux WPA/WPA2/IEEE 802.1X Supplicant"
DESCRIPTION="
wpa_supplicant is a WPA Supplicant for Linux, BSD, Mac OS X, and Windows with
support for WPA and WPA2 (IEEE 802.11i / RSN). It is suitable for both
desktop/laptop computers and embedded systems. Supplicant is the IEEE
802.1X/WPA component that is used in the client stations. It implements key
negotiation with a WPA Authenticator and it controls the roaming and IEEE
802.11 authentication/association of the wlan driver.

wpa_supplicant is designed to be a daemon program that runs in the background
and acts as the backend component controlling the wireless connection.
wpa_supplicant supports separate frontend programs and a text-based frontend
(wpa_cli) and a GUI (wpa_gui) are included with wpa_supplicant.
"
HOMEPAGE="https://w1.fi/${PN}"

LICENCES="|| ( BSD-3 GPL-2 )"
SLOT="0"

MYOPTIONS="
    dbus     [[ description = [ Enable the DBus control interface ] ]]
    pcap     [[ description = [
        Use libdnet/libpcap for Layer 2 processing instead of Linux packet sockets
    ] ]]
    pcsc     [[ description = [
        Include GSM SIM/USIM interface for GSM/UMTS authentication algorithm (for EAP-SIM/EAP-AKA)
    ] ]]
    qt5      [[ description = [ Build the Qt-based wpa_gui program ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# FIXME: Tests don't seem to get along with external crypto -- needs further
# investigation. Be sure to uncomment *all* test-related code below when this
# is resolved.
RESTRICT="test"

DEPENDENCIES="
    build:
        dbus? ( virtual/pkg-config )
        qt5? (
            x11-libs/qttools:5 [[ note = [ lrelease/lconvert for translations ] ]]
        )
    build+run:
        net-libs/libnl:3.0
        sys-libs/ncurses
        sys-libs/readline:=
        dbus? ( sys-apps/dbus )
        pcap? (
            dev-libs/libdnet
            dev-libs/libpcap
        )
        pcsc? ( sys-apps/pcsc-lite )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        qt5? (
            x11-libs/qtbase:5[gui]
            x11-libs/qtsvg:5
        )
"

if ever is_scm ; then
    DEPENDENCIES+="
        build:
            app-text/docbook-sgml-dtd:4.1
            app-text/docbook-utils
    "
fi

WORK=${WORKBASE}/${PNV}/${PN}

EQMAKE_SOURCES="wpa_gui.pro"

DEFAULT_SRC_COMPILE_PARAMS=(
    CC="${CC}"
    BINDIR=/usr/$(exhost --target)/bin
    INCDIR=/usr/$(exhost --target)/include
    LIBDIR=/usr/$(exhost --target)/lib
    V=1
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( eap_testing.txt wpa_supplicant.conf )

# Enable CONFIG_* options
config_enable() {
    for config in ${@} ; do
        local str="CONFIG_${config}"
        [[ ${str} =~ = ]] || str="${str}=y"

        echo "echo \"${str}\" >> .config" 1>&2
        echo "${str}" >> .config || die
    done
}

# Enable CONFIG_DRIVER_* options
config_enable_driver() {
    for driver in ${@} ; do
        config_enable "DRIVER_${driver}"
    done
}

# Enable CONFIG_EAP_* options
config_enable_eap() {
    for eap in ${@} ; do
        config_enable "EAP_${eap}"
    done
}

wpa_supplicant_src_prepare() {
    edo pushd ${WORKBASE}/${PNV}
    default
    edo popd

    # FILES is a reserved exheres-0 variable
    ever is_scm && edo sed -i -e 's:FILES:DOC_FILES:g' doc/docbook/Makefile

    # Fix PCSC include dir
    edo sed -i -e "s:/usr/include/PCSC:/usr/$(exhost --target)/include/PCSC:g" Makefile
}

wpa_supplicant_src_configure() {
    config_enable \
        AP \
        BACKEND=file \
        CTRL_IFACE=unix \
        DEBUG_FILE \
        ELOOP=eloop \
        GETRANDOM \
        HT_OVERRIDES \
        IBSS_RSN \
        IEEE80211AC \
        IEEE80211AX \
        IEEE80211N \
        IEEE80211R \
        IEEE80211W \
        IEEE8021X_EAPOL \
        LIBNL32 \
        MAIN=main \
        MATCH_IFACE \
        OS=unix \
        PKCS12 \
        READLINE \
        SMARTCARD \
        TDLS \
        TLS=openssl \
        TLSV11 \
        TLSV12 \
        VHT_OVERRIDES

    option dbus     && config_enable CTRL_IFACE_DBUS{_NEW,_INTRO}
    option pcap     && config_enable L2_PACKET=pcap ||
                       config_enable L2_PACKET=linux

    # EAP_AKA, EAP_AKA_PRIME, and EAP_SIM require PCSC. USIM_SIMULATOR is used
    # by EAP_AKA.
    if option pcsc ; then
        config_enable \
            PCSC \
            SIM_SIMULATOR \
            USIM_SIMULATOR

        config_enable_eap \
            AKA \
            AKA_PRIME \
            SIM
    fi

#    if option build_options:recommended_tests ; then
#        config_enable \
#            CLIENT_MLME \
#            EAPOL_TEST
#    fi

    # NSA Suite B encryption, doesn't build with LibreSSL, upstream also
    # disables it in their test configs, e.g. compare
    # https://w1.fi/cgit/hostap/tree/tests/build/build-wpa_supplicant-openssl-1.1.1.config#n26
    # https://w1.fi/cgit/hostap/tree/tests/build/build-wpa_supplicant-libressl-2.8.config#n26
    option providers:openssl && config_enable SUITEB SUITEB192

    config_enable_driver \
        ATMEL \
        HOSTAP \
        NL80211 \
        RALINK \
        ROBOSWITCH \
        WEXT \
        WIRED

    # EAP_PEAP, EAP_TLS, and EAP_TTLS require TLS (which is always enabled)
    config_enable_eap \
        EKE \
        FAST \
        GPSK \
        GPSK_SHA256 \
        GTC \
        IKEV2 \
        LEAP \
        MD5 \
        MSCHAPV2 \
        OTP \
        PAX \
        PEAP \
        PSK \
        SAKE \
        TEAP \
        TLS \
        TTLS

    # EAP Re-Authentication Protocol (ERP)
    config_enable ERP

    # Enabling this is a PITA, especially for the user. For details see the
    # file 'wpa_supplicant/defconfig' in the source.
    #config_enable DYNAMIC_EAP_METHODS

    # Enable privilege separation (see README)
    #config_enable PRIVSEP

    # Enable mitigation against certain attacks against TKIP by delaying
    # Michael MIC error reports by a random amount of time between 0 and 60
    # seconds.
    #config_enable DELAYED_MIC_ERROR_REPORT

    # ACS (Automatic Channel Selection)
    config_enable ACS

    # Background scanning
    config_enable BGSCAN_LEARN
    config_enable BGSCAN_SIMPLE

    # IEEE 802.11u Interworking and Wi-Fi Hotspot 2.0
    config_enable HS20
    config_enable INTERWORKING

    # MBO (Multi Band Operation)
    config_enable MBO

    # WNM (Wireless Network Management)
    config_enable WNM

    # WPS (Wi-Fi Protected Setup)
    config_enable WPS
    config_enable WPS_ER
    config_enable WPS_UPNP
    config_enable WPS_NFC

    # P2P (Wi-Fi Direct)
    config_enable P2P

    # Wi-Fi Display
    config_enable WIFI_DISPLAY

    # DPP (Wi-Fi Device Provisioning Protocol)
    config_enable DPP

    # FILS (IEEE 802.11ai) shared key authentication
    config_enable FILS
    config_enable FILS_SK_PFS

    # OWE (Opportunistic Wireless Encryption, RFC 8110; and transition mode defined by WFA)
    config_enable OWE

    # External persistent storage of PMKSA cache
    config_enable PMKSA_CACHE_EXTERNAL

    # Support Operating Channel Validation
    config_enable OCV

    # Device Provisioning Protocol (DPP)
    config_enable DPP

    # Mesh Networking (IEEE 802.11s)
    config_enable MESH

    # Simultaneous Authentication of Equals (SAE), WPA3-Personal
    config_enable SAE

    if option qt5 ; then
        edo pushd wpa_gui-qt4
        qmake_src_configure
        edo popd
    fi
}

wpa_supplicant_src_compile() {
    default

    if option qt5 ; then
        edo pushd wpa_gui-qt4
        edo lrelease-qt5 lang/wpa_gui_*.ts
        emake
        edo popd
    fi

    ever is_scm && emake -C doc/docbook man
}

#src_test() {
#    emake tests
#}

wpa_supplicant_src_install() {
    dobin wpa_cli wpa_passphrase wpa_supplicant

    if option dbus ; then
        pushd dbus

        insinto /usr/share/dbus-1/system.d/
        newins {dbus-,}wpa_supplicant.conf

        # Will probably need to be moved to session services if privilege separation is implemented
        insinto /usr/share/dbus-1/system-services/
        doins 'fi.w1.wpa_supplicant1.service'

        popd
    fi

    if option qt5 ; then
        edo pushd wpa_gui-qt4

        dobin wpa_gui

        insinto /usr/share/qt5/translations
        doins lang/*.qm

        insinto /usr/share/applications
        doins wpa_gui.desktop

        insinto /usr/share/icons/hicolor/scalable/apps
        doins icons/wpa_gui.svg

        edo popd
    fi

    insinto "${SYSTEMDSYSTEMUNITDIR}"
    doins "${WORK}"/systemd/{wpa_supplicant@,wpa_supplicant-nl80211@,wpa_supplicant-wired@}.service
    option dbus && doins "${WORK}"/systemd/wpa_supplicant.service

    emagicdocs
    doman doc/docbook/*.{5,8}
    install_openrc_files

    # PRIVILEGE SEPARATION
#    dobin wpa_priv

    # also require the following
    # user/${PN}
    # group/${PN}
    # group/wpa_priv
}

wpa_supplicant_pkg_postinst() {
    option qt5 && gtk-icon-cache_pkg_postinst

#    elog "In order to use a control interface group with wpa_supplicant"
#    elog "Please add the system-wide wpa_supplicant user to that group:"
#    elog " usermod -a -G <control_interface_group> wpa_supplicant"
}

wpa_supplicant_pkg_postrm() {
    option qt5 && gtk-icon-cache_pkg_postrm
}

