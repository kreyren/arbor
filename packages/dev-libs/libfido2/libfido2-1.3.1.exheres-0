# Copyright 2020 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

require github [ user="Yubico" tag="${PV}" ]
require cmake

SUMMARY="Provides library functionality for FIDO 2.0, including communication with a device over USB"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    doc
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        doc? ( sys-apps/mandoc )
    build+run:
        dev-libs/libcbor
        providers:eudev? ( sys-apps/eudev )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        providers:systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Use-GNUInstallDirs-to-install-documentation-and-manp.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DCMAKE_INSTALL_MANDIR:PATH=/usr/share/man
)

src_configure() {
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DMANDOC_PATH:PATH=$(option doc /usr/$(exhost --target)/bin/mandoc FALSE)
    )

    cmake_src_configure
}

