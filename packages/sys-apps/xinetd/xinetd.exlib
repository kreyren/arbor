# Copyright 2008 Daniel Mierswa <impulze@impulze.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=openSUSE release=${PV} suffix=tar.xz ] \
    flag-o-matic \
    systemd-service

export_exlib_phases src_prepare src_configure src_install

SUMMARY="${PN} is a super-server daemon and a safe replacement for inetd"
DESCRIPTION="${PN} listens on specific ports to launch programs configured for that port."

LICENCES="${PN}"
SLOT="0"
MYOPTIONS="
    perl
    rpc
    tcpd
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        rpc? ( net-libs/libtirpc )
        tcpd? ( sys-apps/tcp-wrappers )
    run:
        perl? ( dev-lang/perl:* )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --disable-werror
    --with-loadavg
    --without-labeled-networking
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    rpc
    'tcpd libwrap'
)

xinetd_src_prepare() {
    default

    edo sed \
        -e '/only_from/s/^#//g' \
        -i contrib/xinetd.conf
}

xinetd_src_configure() {
    # Fix build with musl.
    if [[ "$(exhost --target)" == *-musl* ]]; then
        append-cppflags -DHAVE_RLIM_T
        export xinetd_cv_type_socklen_t=1
    fi

    default
}

xinetd_src_install() {
    default

    insinto /etc
    doins -r "${WORK}"/contrib/xinetd.d
    doins "${WORK}"/contrib/xinetd.conf

    install_systemd_files

    if ! option perl ; then
        edo rm "${IMAGE}"/usr/$(exhost --target)/bin/xconv.pl
        edo rm "${IMAGE}"/usr/share/man/man8/xconv.pl.8
    fi
}

