From 3538fafb471478453ca80f6ebd8b4e980909f4cf Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Zbigniew=20J=C4=99drzejewski-Szmek?= <zbyszek@in.waw.pl>
Date: Mon, 3 Feb 2020 20:38:54 +0100
Subject: [PATCH 092/104] meson: update efi path detection to gnu-efi-3.0.11

Fixes systemd build in Fedora rawhide.

The old ldsdir option is not useful, because both the directory and the
file name changed. Let's remove the option and try to autodetect the file
name. If this turns out to be not enough, a new option to simply specify
the full path to the file can be added.

F31:
         efi arch:                          x86_64
         EFI machine type:                  x64
         EFI CC                             ccache cc
         EFI lds:                           /usr/lib64/gnuefi/elf_x64_efi.lds
         EFI crt0:                          /usr/lib64/gnuefi/crt0-efi-x64.o
         EFI include directory:             /usr/include/efi
F32:
         efi arch:                          x86_64
         EFI machine type:                  x64
         EFI CC                             ccache cc
         EFI lds:                           /usr/lib/gnuefi/x64/efi.lds
         EFI crt0:                          /usr/lib/gnuefi/x64/crt0.o
         EFI include directory:             /usr/include/efi
(cherry picked from commit ce4121c6ff92c1c368874bd451b73fa9b1ddec4a)
---
 meson.build              |  4 +--
 meson_options.txt        |  2 --
 src/boot/efi/meson.build | 63 ++++++++++++++++++++++++++--------------
 3 files changed, 43 insertions(+), 26 deletions(-)

diff --git a/meson.build b/meson.build
index 21d6968abd..1bdb9c7b32 100644
--- a/meson.build
+++ b/meson.build
@@ -3170,8 +3170,8 @@ if conf.get('ENABLE_EFI') == 1
                 status += [
                         'EFI machine type:                  @0@'.format(EFI_MACHINE_TYPE_NAME),
                         'EFI CC                             @0@'.format(' '.join(efi_cc)),
-                        'EFI lib directory:                 @0@'.format(efi_libdir),
-                        'EFI lds directory:                 @0@'.format(efi_ldsdir),
+                        'EFI lds:                           @0@'.format(efi_lds),
+                        'EFI crt0:                          @0@'.format(efi_crt0),
                         'EFI include directory:             @0@'.format(efi_incdir)]
         endif
 endif
diff --git a/meson_options.txt b/meson_options.txt
index 79b09e021c..44ff23f641 100644
--- a/meson_options.txt
+++ b/meson_options.txt
@@ -309,8 +309,6 @@ option('efi-ld', type : 'string',
        description : 'the linker to use for EFI modules')
 option('efi-libdir', type : 'string',
        description : 'path to the EFI lib directory')
-option('efi-ldsdir', type : 'string',
-       description : 'path to the EFI lds directory')
 option('efi-includedir', type : 'string', value : '/usr/include/efi',
        description : 'path to the EFI header directory')
 option('tpm-pcrindex', type : 'integer', value : 8,
diff --git a/src/boot/efi/meson.build b/src/boot/efi/meson.build
index 3edabfedd5..c1fe04597b 100644
--- a/src/boot/efi/meson.build
+++ b/src/boot/efi/meson.build
@@ -64,12 +64,19 @@ if conf.get('ENABLE_EFI') == 1 and get_option('gnu-efi') != 'false'
 
         efi_libdir = get_option('efi-libdir')
         if efi_libdir == ''
-                ret = run_command(efi_cc + ['-print-multi-os-directory'])
-                if ret.returncode() == 0
-                        path = join_paths('/usr/lib', ret.stdout().strip())
-                        ret = run_command('realpath', '-e', path)
-                        if ret.returncode() == 0
-                                efi_libdir = ret.stdout().strip()
+                # New location first introduced with gnu-efi 3.0.11
+                efi_libdir = join_paths('/usr/lib/gnuefi', EFI_MACHINE_TYPE_NAME)
+                cmd = run_command('test', '-e', efi_libdir)
+
+                if cmd.returncode() != 0
+                        # Fall back to the old approach
+                        cmd = run_command(efi_cc + ['-print-multi-os-directory'])
+                        if cmd.returncode() == 0
+                                path = join_paths('/usr/lib', cmd.stdout().strip())
+                                cmd = run_command('realpath', '-e', path)
+                                if cmd.returncode() == 0
+                                        efi_libdir = cmd.stdout().strip()
+                                endif
                         endif
                 endif
         endif
@@ -95,20 +102,35 @@ if have_gnu_efi
 
         objcopy = find_program('objcopy')
 
-        efi_ldsdir = get_option('efi-ldsdir')
-        arch_lds = 'elf_@0@_efi.lds'.format(gnu_efi_path_arch)
-        if efi_ldsdir == ''
-                efi_ldsdir = join_paths(efi_libdir, 'gnuefi')
-                cmd = run_command('test', '-f', join_paths(efi_ldsdir, arch_lds))
-                if cmd.returncode() != 0
-                        efi_ldsdir = efi_libdir
-                        cmd = run_command('test', '-f', join_paths(efi_ldsdir, arch_lds))
-                        if cmd.returncode() != 0
-                               error('Cannot find @0@'.format(arch_lds))
+        efi_location_map = [
+                # New locations first introduced with gnu-efi 3.0.11
+                [join_paths(efi_libdir, 'efi.lds'),
+                 join_paths(efi_libdir, 'crt0.o')],
+                # Older locations...
+                [join_paths(efi_libdir, 'gnuefi', 'elf_@0@_efi.lds'.format(gnu_efi_path_arch)),
+                 join_paths(efi_libdir, 'gnuefi', 'crt0-efi-@0@.o'.format(gnu_efi_path_arch))],
+                [join_paths(efi_libdir, 'elf_@0@_efi.lds'.format(gnu_efi_path_arch)),
+                 join_paths(efi_libdir, 'crt0-efi-@0@.o'.format(gnu_efi_path_arch))]]
+        efi_lds = ''
+        foreach location : efi_location_map
+                if efi_lds == ''
+                        cmd = run_command('test', '-f', location[0])
+                        if cmd.returncode() == 0
+                                efi_lds = location[0]
+                                efi_crt0 = location[1]
                         endif
                 endif
+        endforeach
+        if efi_lds == ''
+                if get_option('gnu-efi') == 'true'
+                        error('gnu-efi support requested, but cannot find efi.lds')
+                else
+                        have_gnu_efi = false
+                endif
         endif
+endif
 
+if have_gnu_efi
         compile_args = ['-Wall',
                         '-Wextra',
                         '-std=gnu90',
@@ -145,14 +167,13 @@ if have_gnu_efi
                 compile_args += ['-O2']
         endif
 
-        efi_ldflags = ['-T',
-                       join_paths(efi_ldsdir, arch_lds),
+        efi_ldflags = ['-T', efi_lds,
                        '-shared',
                        '-Bsymbolic',
                        '-nostdlib',
                        '-znocombreloc',
                        '-L', efi_libdir,
-                       join_paths(efi_ldsdir, 'crt0-efi-@0@.o'.format(gnu_efi_path_arch))]
+                       efi_crt0]
         if efi_arch == 'aarch64' or efi_arch == 'arm'
                 # Aarch64 and ARM32 don't have an EFI capable objcopy. Use 'binary'
                 # instead, and add required symbols manually.
@@ -219,11 +240,9 @@ if have_gnu_efi
                 set_variable(tuple[0].underscorify(), so)
                 set_variable(tuple[0].underscorify() + '_stub', stub)
         endforeach
-endif
 
-############################################################
+        ############################################################
 
-if have_gnu_efi
         test_efi_disk_img = custom_target(
                 'test-efi-disk.img',
                 input : [systemd_boot_so, stub_so_stub],
-- 
2.25.1

