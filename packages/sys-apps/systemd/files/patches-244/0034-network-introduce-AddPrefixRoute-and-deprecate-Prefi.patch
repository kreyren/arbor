From 93074c962e3a17e498f12e82177ee581f9ad3526 Mon Sep 17 00:00:00 2001
From: Yu Watanabe <watanabe.yu+github@gmail.com>
Date: Sun, 8 Dec 2019 00:32:36 +0900
Subject: [PATCH 034/104] network: introduce AddPrefixRoute= and deprecate
 PrefixRoute=

PrefixRoute= was added by e63be0847c39bfdca45c25c505922814374581a7,
but unfortunately, the meaning of PrefixRoute= is inverted; when true
IFA_F_NOPREFIXROUTE flag is added. This introduces AddPrefixRoute=
setting.

(cherry picked from commit de697db05b08464180af4a257f6df88b00d60b9e)
---
 man/systemd.network.xml                          |  8 +++-----
 src/network/networkd-address.c                   |  5 ++++-
 src/network/networkd-dhcp4.c                     | 14 +++++++-------
 src/network/networkd-network-gperf.gperf         |  3 ++-
 test/fuzz/fuzz-network-parser/directives.network |  1 +
 5 files changed, 17 insertions(+), 14 deletions(-)

diff --git a/man/systemd.network.xml b/man/systemd.network.xml
index a26e08c99c..8254d7ca1f 100644
--- a/man/systemd.network.xml
+++ b/man/systemd.network.xml
@@ -1004,12 +1004,10 @@
           </listitem>
         </varlistentry>
         <varlistentry>
-          <term><varname>PrefixRoute=</varname></term>
+          <term><varname>AddPrefixRoute=</varname></term>
           <listitem>
-            <para>Takes a boolean. When adding or modifying an IPv6 address, the userspace
-            application needs a way to suppress adding a prefix route. This is for example relevant
-            together with IFA_F_MANAGERTEMPADDR, where userspace creates autoconf generated addresses,
-            but depending on on-link, no route for the prefix should be added. Defaults to false.</para>
+            <para>Takes a boolean. When true, the prefix route for the address is automatically added.
+            Defaults to true.</para>
           </listitem>
         </varlistentry>
         <varlistentry>
diff --git a/src/network/networkd-address.c b/src/network/networkd-address.c
index 23d40ccc41..8579771941 100644
--- a/src/network/networkd-address.c
+++ b/src/network/networkd-address.c
@@ -32,6 +32,7 @@ int address_new(Address **ret) {
                 .scope = RT_SCOPE_UNIVERSE,
                 .cinfo.ifa_prefered = CACHE_INFO_INFINITY_LIFE_TIME,
                 .cinfo.ifa_valid = CACHE_INFO_INFINITY_LIFE_TIME,
+                .prefix_route = true,
         };
 
         *ret = TAKE_PTR(address);
@@ -593,7 +594,7 @@ int address_configure(
         if (address->manage_temporary_address)
                 address->flags |= IFA_F_MANAGETEMPADDR;
 
-        if (address->prefix_route)
+        if (!address->prefix_route)
                 address->flags |= IFA_F_NOPREFIXROUTE;
 
         if (address->autojoin)
@@ -908,6 +909,8 @@ int config_parse_address_flags(const char *unit,
         else if (streq(lvalue, "ManageTemporaryAddress"))
                 n->manage_temporary_address = r;
         else if (streq(lvalue, "PrefixRoute"))
+                n->prefix_route = !r;
+        else if (streq(lvalue, "AddPrefixRoute"))
                 n->prefix_route = r;
         else if (streq(lvalue, "AutoJoin"))
                 n->autojoin = r;
diff --git a/src/network/networkd-dhcp4.c b/src/network/networkd-dhcp4.c
index 8ca87d99d4..95c8b92541 100644
--- a/src/network/networkd-dhcp4.c
+++ b/src/network/networkd-dhcp4.c
@@ -111,10 +111,10 @@ static int route_scope_from_address(const Route *route, const struct in_addr *se
                 return RT_SCOPE_UNIVERSE;
 }
 
-static bool link_noprefixroute(Link *link) {
-        return link->network->dhcp_route_table_set &&
-                link->network->dhcp_route_table != RT_TABLE_MAIN &&
-                !link->manager->dhcp4_prefix_root_cannot_set_table;
+static bool link_prefixroute(Link *link) {
+        return !link->network->dhcp_route_table_set ||
+                link->network->dhcp_route_table == RT_TABLE_MAIN ||
+                link->manager->dhcp4_prefix_root_cannot_set_table;
 }
 
 static int dhcp_route_configure(Route **route, Link *link) {
@@ -254,7 +254,7 @@ static int link_set_dhcp_routes(Link *link) {
         if (r < 0)
                 return log_link_warning_errno(link, r, "DHCP error: could not get address: %m");
 
-        if (link_noprefixroute(link)) {
+        if (!link_prefixroute(link)) {
                 _cleanup_(route_freep) Route *prefix_route = NULL;
 
                 r = dhcp_prefix_route_from_lease(link->dhcp_lease, table, &address, &prefix_route);
@@ -516,7 +516,7 @@ static int dhcp_remove_dns_routes(Link *link, sd_dhcp_lease *lease, const struct
                 (void) route_remove(route, link, NULL);
         }
 
-        if (link_noprefixroute(link)) {
+        if (!link_prefixroute(link)) {
                 _cleanup_(route_freep) Route *prefix_route = NULL;
 
                 r = dhcp_prefix_route_from_lease(lease, table, address, &prefix_route);
@@ -719,7 +719,7 @@ static int dhcp4_update_address(Link *link,
         addr->cinfo.ifa_valid = lifetime;
         addr->prefixlen = prefixlen;
         addr->broadcast.s_addr = address->s_addr | ~netmask->s_addr;
-        addr->prefix_route = link_noprefixroute(link);
+        addr->prefix_route = link_prefixroute(link);
 
         /* allow reusing an existing address and simply update its lifetime
          * in case it already exists */
diff --git a/src/network/networkd-network-gperf.gperf b/src/network/networkd-network-gperf.gperf
index f314b1ec16..f7e68be181 100644
--- a/src/network/networkd-network-gperf.gperf
+++ b/src/network/networkd-network-gperf.gperf
@@ -107,7 +107,8 @@ Address.PreferredLifetime,              config_parse_lifetime,
 Address.HomeAddress,                    config_parse_address_flags,                      0,                             0
 Address.DuplicateAddressDetection,      config_parse_address_flags,                      0,                             0
 Address.ManageTemporaryAddress,         config_parse_address_flags,                      0,                             0
-Address.PrefixRoute,                    config_parse_address_flags,                      0,                             0
+Address.PrefixRoute,                    config_parse_address_flags,                      0,                             0 /* deprecated */
+Address.AddPrefixRoute,                 config_parse_address_flags,                      0,                             0
 Address.AutoJoin,                       config_parse_address_flags,                      0,                             0
 Address.Scope,                          config_parse_address_scope,                      0,                             0
 IPv6AddressLabel.Prefix,                config_parse_address_label_prefix,               0,                             0
diff --git a/test/fuzz/fuzz-network-parser/directives.network b/test/fuzz/fuzz-network-parser/directives.network
index cb10ca306a..5bd80dece8 100644
--- a/test/fuzz/fuzz-network-parser/directives.network
+++ b/test/fuzz/fuzz-network-parser/directives.network
@@ -202,6 +202,7 @@ Address=
 Scope=
 HomeAddress=
 PrefixRoute=
+AddPrefixRoute=
 ManageTemporaryAddress=
 Broadcast=
 Peer=
-- 
2.25.1

